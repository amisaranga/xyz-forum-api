<?php

namespace App\Models;

use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $fillable = [
        'content',
        'post_id',
        'user_id',
    ];

    /**
	 * A Comment belongs to a User
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
	 * A Comment belongs to a Post
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
