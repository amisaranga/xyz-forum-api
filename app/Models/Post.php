<?php

namespace App\Models;

use App\Models\Comment;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    const STATUS_NEW = 0;
    const STATUS_APPROVED = 1;

    protected $with = ['user'];

    protected $fillable = [
        'title',
        'body',
        'user_id',
        'status',
    ];

    /**
	 * A Post belongs to a User
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
	 * A Post has many Comments
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
