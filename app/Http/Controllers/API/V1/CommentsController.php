<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Post;
use App\Http\Resources\Comment as CommentResource;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    /**
     * @api {get} /posts/{id}/comments Get Comments
     * @apiVersion 1.0.0
     * @apiName GetComments
     * @apiGroup Comments
     *
     * @apiDescription
     * Get the comments of a post identified by `id`.
     *
     * @apiHeader {String} Authorization [Access token](#success-examples-Auth-GetAccessToken-1_0_0-0) in the format: `Bearer {access-token}`
     * @apiHeader {String} Accept `application/json`
     *
     * @apiParamExample {url} Request Example
     * http://localhost:8000/api/v1/posts/34/comments
     *
     * @apiSuccess {Object[]} data List of Comments. See [Comment details](#api-Comments-GetComment)
     *
     * @apiSuccessExample {json} Success Response
     *  HTTP/1.1 200 OK
     *  {
     *      "data": [
     *          {... comment ...},
     *          {... comment ...}
     *      ]
     *  }
     */
    public function postComments(Request $request, Post $post)
    {
        $commentsQuery = Comment::with('user')->orderBy('created_at', 'desc');

        return CommentResource::collection($post->comments);
    }

    /**
     * @api {comment} /posts/{id}/comments Add New Comment
     * @apiVersion 1.0.0
     * @apiName CreateComment
     * @apiGroup Comments
     *
     * @apiDescription
     * Add a new comment to a post.
     *
     * @apiHeader {String} Authorization [Access token](#success-examples-Auth-GetAccessToken-1_0_0-0) in the format: `Bearer {access-token}`
     * @apiHeader {String} Content-Type `application/json`
     * @apiHeader {String} Accept `application/json`
     *
     * @apiParam {Integer} id ID of the post
     * @apiParam {String} content Content of the comment
     *
     * @apiParamExample {json} Request Example
     * {
     *     "content":"Possimus excepturi voluptatem unde incidunt debitis optio...",
     * }
     *
     * @apiSuccess {Object} data Comment details
     * @apiSuccess {Integer} data.id ID of the comment
     * @apiSuccess {String} data.content Content of the comment
     * @apiSuccess {String} data.created_at Comment created time in the format `03 Jul 2021 12:52 pm`
     * @apiSuccess {Integer} data.post_id ID of the post that the comment belongs to
     * @apiSuccess {Integer} data.user_id User ID of the comment owner
     * @apiSuccess {String} data.user Name of the comment owner
     *
     * @apiSuccessExample {json} Success Response
     * {
     *    "data":{
     *       "id":1,
     *       "content":"Possimus excepturi voluptatem unde incidunt debitis optio...",
     *       "created_at":"03 Jul 2021 12:52 pm",
     *       "post_id":25,
     *       "user_id":2,
     *       "user":"Mr. Johnathan Durgan MD"
     *    }
     * }
     */
    public function store(Request $request, Post $post)
    {
        $this->validate($request, [
            'content' => 'required',
        ]);

        $data = $request->only(['content']);
        $data['user_id'] = $request->user()->id;
        $comment = $post->comments()->create($data);

        return new CommentResource($comment);
    }

    /**
     * @api {get} /comments/{id} Get Comment
     * @apiVersion 1.0.0
     * @apiName GetComment
     * @apiGroup Comments
     *
     * @apiDescription
     * Get comment details.
     *
     * @apiHeader {String} Authorization [Access token](#success-examples-Auth-GetAccessToken-1_0_0-0) in the format: `Bearer {access-token}`
     * @apiHeader {String} Accept `application/json`
     *
     * @apiParam {String} id ID of the comment
     *
     * @apiSuccess {Object} data Comment details
     * @apiSuccess {Integer} data.id ID of the comment
     * @apiSuccess {String} data.content Content of the comment
     * @apiSuccess {String} data.created_at Comment created time in the format `03 Jul 2021 12:52 pm`
     * @apiSuccess {Integer} data.post_id ID of the post that the comment belongs to
     * @apiSuccess {Integer} data.user_id User ID of the comment owner
     * @apiSuccess {String} data.user Name of the comment owner
     *
     * @apiSuccessExample {json} Success Response
     * {
     *    "data":{
     *       "id":1,
     *       "content":"Possimus excepturi voluptatem unde incidunt debitis optio...",
     *       "created_at":"03 Jul 2021 12:52 pm",
     *       "post_id":25,
     *       "user_id":2,
     *       "user":"Mr. Johnathan Durgan MD"
     *    }
     * }
     */
    public function show(Comment $comment)
    {
        return new CommentResource($comment);
    }

    /**
     * @api {put} /comments/{id} Update Comment
     * @apiVersion 1.0.0
     * @apiName UpdateComment
     * @apiGroup Comments
     *
     * @apiDescription
     * Update comment details.
     *
     * @apiHeader {String} Authorization [Access token](#success-examples-Auth-GetAccessToken-1_0_0-0) in the format: `Bearer {access-token}`
     * @apiHeader {String} Content-Type `application/json`
     * @apiHeader {String} Accept `application/json`
     *
     * @apiParam {Integer} id ID of the comment
     * @apiParam {String} content Content of the comment
     *
     * @apiParamExample {json} Request Example
     * {
     *     "content":"Possimus excepturi voluptatem unde incidunt debitis optio...",
     * }
     *
     * @apiSuccess {Object} data Comment details
     * @apiSuccess {Integer} data.id ID of the comment
     * @apiSuccess {String} data.content Content of the comment
     * @apiSuccess {String} data.created_at Comment created time in the format `03 Jul 2021 12:52 pm`
     * @apiSuccess {Integer} data.post_id ID of the post that the comment belongs to
     * @apiSuccess {Integer} data.user_id User ID of the comment owner
     * @apiSuccess {String} data.user Name of the comment owner
     *
     * @apiSuccessExample {json} Success Response
     * {
     *    "data":{
     *       "id":1,
     *       "content":"Possimus excepturi voluptatem unde incidunt debitis optio...",
     *       "created_at":"03 Jul 2021 12:52 pm",
     *       "post_id":25,
     *       "user_id":2,
     *       "user":"Mr. Johnathan Durgan MD"
     *    }
     * }
     */
    public function update(Request $request, Comment $comment)
    {
        $this->validate($request, [
            'content' => 'sometimes|required',
        ]);

        $data = $request->only(['content']);
        $comment->update($data);

        return new CommentResource($comment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        //
    }
}
