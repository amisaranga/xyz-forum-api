<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\User as UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    /**
     * @api {post} /auth/register Register
     * @apiVersion 1.0.0
     * @apiName RegisterUser
     * @apiGroup Auth
     *
     * @apiDescription
     * Registers a new user.
     *
     * @apiHeader {String} Content-Type `application/json`
     * @apiHeader {String} Accept `application/json`
     *
     * @apiParam {String} name Name of the user
     * @apiParam {String} email Email address of the user
     * @apiParam {String} password Password
     *
     * @apiParamExample {json} Request Example
     * {
     *      "name":"John Doe",
     *      "email":"john@xyz.com",
     *      "password":"12345678"
     * }
     *
     * @apiSuccess {String} access_token Access token
     * @apiSuccess {String} user [User details](#api-Auth-GetUser)
     *
     * @apiSuccessExample {json} Success Response
     *  HTTP/1.1 200 OK
     *  {
     *      "access_token": "eyJ0eXAiOiJKV1QiLC . . . bDc25OSDm3CzWphUZBYby9gske3xk"
     *      "data": { ... user object ... }
     *  }
     *
     */
    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:8',
        ]);

        $user = User::where('email', $request->email)->first();

        if ($user) {
            throw ValidationException::withMessages([
                'email' => ['This email is already registered. Please login to continue.'],
            ]);
        }

        $data = $request->only(['name', 'email', 'password']);
        $data['password'] = bcrypt($data['password']);
        $user = User::create($data);
        $token = $user->createToken($request->email)->plainTextToken;

        return response()->json([
            'access_token' => $token,
            'data' => new UserResource($user),
        ], 200, [
            'Authorization' => 'Bearer ' . $token
        ]);
    }

    /**
     * @api {post} /auth/login Login
     * @apiVersion 1.0.0
     * @apiName GetAccessToken
     * @apiGroup Auth
     *
     * @apiDescription
     * This API endpoint accepts the `email` and `password` of the user
     * and returns an access token for valid user credentials.
     * An access token is used to authorize the requests coming from the users.
     *
     * @apiHeader {String} Content-Type `application/json`
     * @apiHeader {String} Accept `application/json`
     *
     * @apiParam {String} email Username (email) of the user
     * @apiParam {String} password Password of the user
     *
     * @apiParamExample {json} Request Example
     * {
     *      "email":"user@exmpl.com",
     *      "password":"1234",
     * }
     *
     * @apiSuccess {String} access_token Access token
     * @apiSuccess {String} token_type Type of the token. Usually this is a `bearer` token. [See usage instructions](https://tools.ietf.org/html/rfc6750).
     * @apiSuccess {String} expires_in Token expiry time in seconds
     * @apiSuccess {String} data [User details](#api-Auth-GetUser)
     *
     * @apiSuccessExample {json} Response Example
     *  HTTP/1.1 200 OK
     *  {
     *      "access_token": "eyJ0eXAiOiJKV1QiLC . . . bDc25OSDm3CzWphUZBYby9gske3xk"
     *      "token_type": "bearer",
     *      "expires_in": 1296000,
     *      "data": { ... user object ... }
     *  }
     *
     * @apiError 401 Unauthorized
     *
     * @apiErrorExample {json} Invalid User Credentials
     *  HTTP/1.1 401 Unauthorized
     *  {
     *      "error": "Unauthorized"
     *  }
     *
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $user = User::where('email', $request->email)->first();

        if (!$user || !Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are not valid.'],
            ]);
        }

        $token = $user->createToken($request->email)->plainTextToken;

        return response()->json([
            'access_token' => $token,
            'data' => new UserResource($user),
        ], 200, [
            'Authorization' => 'Bearer ' . $token
        ]);
    }

    /**
     * @api {post} /auth/user Get User
     * @apiVersion 1.0.0
     * @apiName GetUser
     * @apiGroup Auth
     *
     * @apiDescription
     * Returns the details of the user identified by the `access-token`
     *
     * @apiHeader {String} Authorization [Access token](#success-examples-Auth-GetAccessToken-1_0_0-0) in the format: `Bearer {access-token}`
     * @apiHeader {String} Accept `application/json`
     *
     * @apiSuccess {String} access_token Access token
     * @apiSuccess {Object} data User details
     * @apiSuccess {Integer} data.id ID of the user
     * @apiSuccess {String} data.name Name of the user
     * @apiSuccess {String} data.email Email of the user
     * @apiSuccess {String} data.type User type, `user` or `admin`
     * @apiSuccess {String} data.created_at User account created time in the format `03 Jul 2021 12:52 pm`
     *
     * @apiSuccessExample {json} Response Example
     * HTTP/1.1 200 OK
     * {
     *   "access_token":null,
     *   "data":{
     *     "id":32,
     *     "name":"John Doe",
     *     "email":"john@xyz.com",
     *     "type":"user",
     *     "created_at":"04 Jul 2021 05:45 pm"
     *   }
     * }
     *
     * @apiError 401 Unauthorized
     *
     * @apiErrorExample {json} Invalid User Credentials
     *  HTTP/1.1 401 Unauthorized
     *  {
     *      "error": "Unauthorized"
     *  }
     *
     */
    public function user(Request $request)
    {
        $user = $request->user();

        if ($user) {
            $token = $user->currentAccessToken();

            if ($token) {
                return response()->json([
                    'access_token' => $token->plainTextToken,
                    'data' => new UserResource($user)
                ], 200, [
                    'Authorization' => 'Bearer ' . $token->plainTextToken
                ]);
            }
        }

        abort(401);
    }
}
