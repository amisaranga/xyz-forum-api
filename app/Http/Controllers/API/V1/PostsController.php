<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\Post as PostResource;
use App\Models\Post;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    /**
     * @api {get} /posts Get Posts
     * @apiVersion 1.0.0
     * @apiName GetPosts
     * @apiGroup Posts
     *
     * @apiDescription
     * Get the list of posts. This will return only the posts accessible to
     * user identified by the provided `access-token`. Pagination is supported
     * using `page` and `per_page` query parameters. Searching is supported
     * using `search` parameter.
     *
     * @apiHeader {String} Authorization [Access token](#success-examples-Auth-GetAccessToken-1_0_0-0) in the format: `Bearer {access-token}`
     * @apiHeader {String} Content-Type `application/json`
     * @apiHeader {String} Accept `application/json`
     *
     * @apiParam {Integer} [page] Number of the page
     * @apiParam {Integer} [per_page] Number of posts per page
     * @apiParam {String} [search] Keywords to search the posts by `title`, `body` & `user name`
     * @apiParam {Integer} [status] Status: `0` for 'New', `1` for `Approved`. This parameter effects only when the requesting user is an `admin`, ignored otherwise.
     *
     * @apiParamExample {url} Request Example
     * http://localhost:8000/api/v1/posts?page=1&per_page=20
     *
     * @apiSuccess {Object[]} data List of Posts. See [Post details](#api-Posts-GetPost)
     * @apiSuccess {Object} links Pagination links. Check [pagination details](#api-_footer) below for more information.
     * @apiSuccess {Object} meta Meta details of current page. Check [pagination details](#api-_footer) below for more information.
     *
     * @apiSuccessExample {json} Success Response
     *  HTTP/1.1 200 OK
     *  {
     *      "data": [
     *          {... post ...},
     *          {... post ...}
     *      ],
     *      "links": {
     *          "first": "http://localhost:8000/api/v1/posts?page=1",
     *          "last": "http://localhost:8000/api/v1/posts?page=1",
     *          "prev": null,
     *          "next": null
     *      },
     *      "meta": {
     *          "current_page": 1,
     *          "from": 1,
     *          "last_page": 1,
     *          "path": "http://localhost:8000/api/v1/posts",
     *          "per_page": 15,
     *          "to": 1,
     *          "total": 1
     *      }
     *  }
     */
    public function index(Request $request)
    {
        $postsQuery = Post::with('user')->orderBy('created_at', 'desc');

        if ($request->filled('search')) {
            $search = $request->query('search');
            $postsQuery->where(function($q) use ($search) {
                $q->where('title', 'LIKE', "%$search%")
                    ->orWhere('body', 'LIKE', "%$search%")
                    ->orWhereHas('user', function($q) use ($search) {
                        $q->where('name', 'LIKE', "%$search%");
                    });
            });
        }

        if ($request->user()->isAdmin) {
            $status = $request->query('status', Post::STATUS_APPROVED);
            $postsQuery->where('status', $status);
        } else {
            $postsQuery->where('status', Post::STATUS_APPROVED);
        }

        $posts = $postsQuery->paginate($request->query('per_page', 10));

        return PostResource::collection($posts);
    }

    /**
     * @api {post} /posts Add New Post
     * @apiVersion 1.0.0
     * @apiName CreatePost
     * @apiGroup Posts
     *
     * @apiDescription
     * Saves post details.
     *
     * @apiHeader {String} Authorization [Access token](#success-examples-Auth-GetAccessToken-1_0_0-0) in the format: `Bearer {access-token}`
     * @apiHeader {String} Content-Type `application/json`
     * @apiHeader {String} Accept `application/json`
     *
     * @apiParam {String} title Post title
     * @apiParam {String} body Content of the post
     *
     * @apiParamExample {json} Request Example
     * {
     *     "title":"Reprehenderit aut atque sed voluptatibus aut.",
     *     "body":"Possimus excepturi voluptatem unde incidunt debitis optio...",
     *  }
     *
     * @apiSuccess {Object} data Post details
     * @apiSuccess {Integer} data.id ID of the post
     * @apiSuccess {String} data.title Post title
     * @apiSuccess {String} data.body Content of the post
     * @apiSuccess {String} data.created_at Post created time in the format `03 Jul 2021 12:52 pm`
     * @apiSuccess {decimal} data.status Status of the post, `0` for `New` and `1` for `Approved`
     * @apiSuccess {Integer} data.user_id User ID of the post owner
     * @apiSuccess {String} data.user Name of the post owner
     *
     * @apiSuccessExample {json} Success Response
     * {
     *    "data":{
     *       "id":1,
     *       "title":"Reprehenderit aut atque sed voluptatibus aut.",
     *       "body":"Possimus excepturi voluptatem unde incidunt debitis optio...",
     *       "created_at":"03 Jul 2021 12:52 pm",
     *       "status":1,
     *       "user_id":2,
     *       "user":"Mr. Johnathan Durgan MD"
     *    }
     * }
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:250',
            'body' => 'required',
        ]);

        $data = $request->only(['title', 'body']);
        $data['status'] = $request->user()->isAdmin ? Post::STATUS_APPROVED : Post::STATUS_NEW;
        $post = $request->user()->posts()->create($data);

        return new PostResource($post);
    }

    /**
     * @api {get} /posts/{id} Get Post
     * @apiVersion 1.0.0
     * @apiName GetPost
     * @apiGroup Posts
     *
     * @apiDescription
     * Get post details.
     *
     * @apiHeader {String} Authorization [Access token](#success-examples-Auth-GetAccessToken-1_0_0-0) in the format: `Bearer {access-token}`
     * @apiHeader {String} Accept `application/json`
     *
     * @apiParam {String} id ID of the post
     *
     * @apiSuccess {Object} data Post details
     * @apiSuccess {Integer} data.id ID of the post
     * @apiSuccess {String} data.title Post title
     * @apiSuccess {String} data.body Content of the post
     * @apiSuccess {String} data.created_at Post created time in the format `03 Jul 2021 12:52 pm`
     * @apiSuccess {decimal} data.status Status of the post, `0` for `New` and `1` for `Approved`
     * @apiSuccess {Integer} data.user_id User ID of the post owner
     * @apiSuccess {String} data.user Name of the post owner
     *
     * @apiSuccessExample {json} Success Response
     * {
     *    "data":{
     *       "id":1,
     *       "title":"Reprehenderit aut atque sed voluptatibus aut.",
     *       "body":"Possimus excepturi voluptatem unde incidunt debitis optio...",
     *       "created_at":"03 Jul 2021 12:52 pm",
     *       "status":1,
     *       "user_id":2,
     *       "user":"Mr. Johnathan Durgan MD"
     *    }
     * }
     */
    public function show(Post $post)
    {
        return new PostResource($post);
    }

    /**
     * @api {put} /posts/{id} Update Post
     * @apiVersion 1.0.0
     * @apiName UpdatePost
     * @apiGroup Posts
     *
     * @apiDescription
     * Update post details.
     *
     * @apiHeader {String} Authorization [Access token](#success-examples-Auth-GetAccessToken-1_0_0-0) in the format: `Bearer {access-token}`
     * @apiHeader {String} Content-Type `application/json`
     * @apiHeader {String} Accept `application/json`
     *
     * @apiParam {String} id ID of the post
     * @apiParam {String} [title] Post title
     * @apiParam {String} [body] Content of the post
     * @apiParam {String} [status] Status of the post, `0` for `New` and `1` for `Approved`. This attribute will be saved only of the requesting user is an `admin`, it will be ignored otherwise.
     *
     * @apiParamExample {json} Request Example
     * {
     *     "title":"Reprehenderit aut atque sed voluptatibus aut.",
     *     "body":"Possimus excepturi voluptatem unde incidunt debitis optio...",
     *     "status":1
     *  }
     *
     * @apiSuccess {Object} data Post details
     * @apiSuccess {Integer} data.id ID of the post
     * @apiSuccess {String} data.title Post title
     * @apiSuccess {String} data.body Content of the post
     * @apiSuccess {String} data.created_at Post created time in the format `03 Jul 2021 12:52 pm`
     * @apiSuccess {decimal} data.status Status of the post, `0` for `New` and `1` for `Approved`
     * @apiSuccess {Integer} data.user_id User ID of the post owner
     * @apiSuccess {String} data.user Name of the post owner
     *
     * @apiSuccessExample {json} Success Response
     * {
     *    "data":{
     *       "id":1,
     *       "title":"Reprehenderit aut atque sed voluptatibus aut.",
     *       "body":"Possimus excepturi voluptatem unde incidunt debitis optio...",
     *       "created_at":"03 Jul 2021 12:52 pm",
     *       "status":1,
     *       "user_id":2,
     *       "user":"Mr. Johnathan Durgan MD"
     *    }
     * }
     */
    public function update(Request $request, Post $post)
    {
        $this->validate($request, [
            'title' => 'sometimes|required|max:250',
            'body' => 'sometimes|required',
        ]);

        $attributes = ['title', 'body'];

        if ($request->user()->isAdmin) {
            $attributes[] = 'status';
        }

        $data = $request->only($attributes);
        $post->update($data);

        return new PostResource($post);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if ($post->delete()) {
            return response()->json([
                'status' => 'success'
            ]);
        }

        abort(500);
    }
}
