# General Instructions

## Pagination

Paginated responses are delivered in following format and parameters

### Paginated response

```json
{
 "data": [
     {},
     {}
 ],
 "links": {
     "first": "http://localhost:8000/api/v1/posts?page=1",
     "last": "http://localhost:8000/api/v1/posts?page=1",
     "prev": null,
     "next": null
 },
 "meta": {
     "current_page": 1,
     "from": 1,
     "last_page": 1,
     "path": "http://localhost:8000/api/v1/posts",
     "per_page": 20,
     "to": 2,
     "total": 2
 }
}
```


### Pagination details
| Field | Type | Description |
| ---- | ---- | ---- |
| links | Object | Pagination links. Check parameters below for details. |
| links.first | String | URL of the first page |
| links.last | String | URL of the last page |
| links.prev | String | URL of the previous page |
| links.next | String | URL of the next page |
| meta | Object | Meta details of current page |
| meta.current_page | Integer | Number of current page |
| meta.from | Integer | Starting offset of the list |
| meta.last_page | Integer | Number of last page |
| meta.path | Integer | Base URL to get the list |
| meta.per_page | Integer | Number of items per page |
| meta.to | Integer | Ending offset of the list |
| meta.total | Integer | Total number of the items in list |
