<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'namespace' => 'App\Http\Controllers\API\V1'], function() {
    Route::group(['prefix' => 'auth'], function() {
        Route::post('/login', 'AuthController@login');
        Route::post('/register', 'AuthController@register');;

        Route::group(['middleware' => 'auth:sanctum'], function() {
            Route::get('/user', 'AuthController@user');
            Route::get('/refresh', 'AuthController@user');
        });
    });

    Route::group(['middleware' => 'auth:sanctum'], function() {
        Route::resource('/posts', 'PostsController');
        Route::get('/posts/{post}/comments', 'CommentsController@postComments')->name('posts.comments');
        Route::post('/posts/{post}/comments', 'CommentsController@store')->name('comments.store');
        Route::resource('/comments', 'CommentsController')->except(['index', 'store']);
    });
});
