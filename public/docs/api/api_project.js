define({
  "name": "XYZ Forum API",
  "version": "1.0.0",
  "description": "Web service to communicate with the backend.",
  "url": "/api/v1",
  "footer": {
    "title": "Usage Instructions",
    "content": "<h1>General Instructions</h1>\n<h2>Pagination</h2>\n<p>Paginated responses are delivered in following format and parameters</p>\n<h3>Paginated response</h3>\n<pre><code class=\"language-json\">{\n &quot;data&quot;: [\n     {},\n     {}\n ],\n &quot;links&quot;: {\n     &quot;first&quot;: &quot;http://localhost:8000/api/v1/posts?page=1&quot;,\n     &quot;last&quot;: &quot;http://localhost:8000/api/v1/posts?page=1&quot;,\n     &quot;prev&quot;: null,\n     &quot;next&quot;: null\n },\n &quot;meta&quot;: {\n     &quot;current_page&quot;: 1,\n     &quot;from&quot;: 1,\n     &quot;last_page&quot;: 1,\n     &quot;path&quot;: &quot;http://localhost:8000/api/v1/posts&quot;,\n     &quot;per_page&quot;: 20,\n     &quot;to&quot;: 2,\n     &quot;total&quot;: 2\n }\n}\n</code></pre>\n<h3>Pagination details</h3>\n<table>\n<thead>\n<tr>\n<th>Field</th>\n<th>Type</th>\n<th>Description</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>links</td>\n<td>Object</td>\n<td>Pagination links. Check parameters below for details.</td>\n</tr>\n<tr>\n<td>links.first</td>\n<td>String</td>\n<td>URL of the first page</td>\n</tr>\n<tr>\n<td>links.last</td>\n<td>String</td>\n<td>URL of the last page</td>\n</tr>\n<tr>\n<td>links.prev</td>\n<td>String</td>\n<td>URL of the previous page</td>\n</tr>\n<tr>\n<td>links.next</td>\n<td>String</td>\n<td>URL of the next page</td>\n</tr>\n<tr>\n<td>meta</td>\n<td>Object</td>\n<td>Meta details of current page</td>\n</tr>\n<tr>\n<td>meta.current_page</td>\n<td>Integer</td>\n<td>Number of current page</td>\n</tr>\n<tr>\n<td>meta.from</td>\n<td>Integer</td>\n<td>Starting offset of the list</td>\n</tr>\n<tr>\n<td>meta.last_page</td>\n<td>Integer</td>\n<td>Number of last page</td>\n</tr>\n<tr>\n<td>meta.path</td>\n<td>Integer</td>\n<td>Base URL to get the list</td>\n</tr>\n<tr>\n<td>meta.per_page</td>\n<td>Integer</td>\n<td>Number of items per page</td>\n</tr>\n<tr>\n<td>meta.to</td>\n<td>Integer</td>\n<td>Ending offset of the list</td>\n</tr>\n<tr>\n<td>meta.total</td>\n<td>Integer</td>\n<td>Total number of the items in list</td>\n</tr>\n</tbody>\n</table>\n"
  },
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2021-07-04T18:12:59.957Z",
    "url": "http://apidocjs.com",
    "version": "0.17.7"
  }
});
