define({ "api": [
  {
    "type": "post",
    "url": "/auth/login",
    "title": "Login",
    "version": "1.0.0",
    "name": "GetAccessToken",
    "group": "Auth",
    "description": "<p>This API endpoint accepts the <code>email</code> and <code>password</code> of the user and returns an access token for valid user credentials. An access token is used to authorize the requests coming from the users.</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p><code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p><code>application/json</code></p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Username (email) of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password of the user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n     \"email\":\"user@exmpl.com\",\n     \"password\":\"1234\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": "<p>Access token</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token_type",
            "description": "<p>Type of the token. Usually this is a <code>bearer</code> token. <a href=\"https://tools.ietf.org/html/rfc6750\">See usage instructions</a>.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "expires_in",
            "description": "<p>Token expiry time in seconds</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": "<p><a href=\"#api-Auth-GetUser\">User details</a></p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response Example",
          "content": "HTTP/1.1 200 OK\n{\n    \"access_token\": \"eyJ0eXAiOiJKV1QiLC . . . bDc25OSDm3CzWphUZBYby9gske3xk\"\n    \"token_type\": \"bearer\",\n    \"expires_in\": 1296000,\n    \"data\": { ... user object ... }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Invalid User Credentials",
          "content": "HTTP/1.1 401 Unauthorized\n{\n    \"error\": \"Unauthorized\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/API/V1/AuthController.php",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "/auth/user",
    "title": "Get User",
    "version": "1.0.0",
    "name": "GetUser",
    "group": "Auth",
    "description": "<p>Returns the details of the user identified by the <code>access-token</code></p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p><a href=\"#success-examples-Auth-GetAccessToken-1_0_0-0\">Access token</a> in the format: <code>Bearer {access-token}</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p><code>application/json</code></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": "<p>Access token</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>User details</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.id",
            "description": "<p>ID of the user</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.name",
            "description": "<p>Name of the user</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.email",
            "description": "<p>Email of the user</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.type",
            "description": "<p>User type, <code>user</code> or <code>admin</code></p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.created_at",
            "description": "<p>User account created time in the format <code>03 Jul 2021 12:52 pm</code></p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response Example",
          "content": "HTTP/1.1 200 OK\n{\n  \"access_token\":null,\n  \"data\":{\n    \"id\":32,\n    \"name\":\"John Doe\",\n    \"email\":\"john@xyz.com\",\n    \"type\":\"user\",\n    \"created_at\":\"04 Jul 2021 05:45 pm\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Invalid User Credentials",
          "content": "HTTP/1.1 401 Unauthorized\n{\n    \"error\": \"Unauthorized\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/API/V1/AuthController.php",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "/auth/register",
    "title": "Register",
    "version": "1.0.0",
    "name": "RegisterUser",
    "group": "Auth",
    "description": "<p>Registers a new user.</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p><code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p><code>application/json</code></p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email address of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n     \"name\":\"John Doe\",\n     \"email\":\"john@xyz.com\",\n     \"password\":\"12345678\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": "<p>Access token</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "user",
            "description": "<p><a href=\"#api-Auth-GetUser\">User details</a></p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"access_token\": \"eyJ0eXAiOiJKV1QiLC . . . bDc25OSDm3CzWphUZBYby9gske3xk\"\n    \"data\": { ... user object ... }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/API/V1/AuthController.php",
    "groupTitle": "Auth"
  },
  {
    "type": "comment",
    "url": "/posts/{id}/comments",
    "title": "Add New Comment",
    "version": "1.0.0",
    "name": "CreateComment",
    "group": "Comments",
    "description": "<p>Add a new comment to a post.</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p><a href=\"#success-examples-Auth-GetAccessToken-1_0_0-0\">Access token</a> in the format: <code>Bearer {access-token}</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p><code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p><code>application/json</code></p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>ID of the post</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>Content of the comment</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"content\":\"Possimus excepturi voluptatem unde incidunt debitis optio...\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Comment details</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.id",
            "description": "<p>ID of the comment</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.content",
            "description": "<p>Content of the comment</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.created_at",
            "description": "<p>Comment created time in the format <code>03 Jul 2021 12:52 pm</code></p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.post_id",
            "description": "<p>ID of the post that the comment belongs to</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.user_id",
            "description": "<p>User ID of the comment owner</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.user",
            "description": "<p>Name of the comment owner</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success Response",
          "content": "{\n   \"data\":{\n      \"id\":1,\n      \"content\":\"Possimus excepturi voluptatem unde incidunt debitis optio...\",\n      \"created_at\":\"03 Jul 2021 12:52 pm\",\n      \"post_id\":25,\n      \"user_id\":2,\n      \"user\":\"Mr. Johnathan Durgan MD\"\n   }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/API/V1/CommentsController.php",
    "groupTitle": "Comments"
  },
  {
    "type": "get",
    "url": "/comments/{id}",
    "title": "Get Comment",
    "version": "1.0.0",
    "name": "GetComment",
    "group": "Comments",
    "description": "<p>Get comment details.</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p><a href=\"#success-examples-Auth-GetAccessToken-1_0_0-0\">Access token</a> in the format: <code>Bearer {access-token}</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p><code>application/json</code></p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>ID of the comment</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Comment details</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.id",
            "description": "<p>ID of the comment</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.content",
            "description": "<p>Content of the comment</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.created_at",
            "description": "<p>Comment created time in the format <code>03 Jul 2021 12:52 pm</code></p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.post_id",
            "description": "<p>ID of the post that the comment belongs to</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.user_id",
            "description": "<p>User ID of the comment owner</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.user",
            "description": "<p>Name of the comment owner</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success Response",
          "content": "{\n   \"data\":{\n      \"id\":1,\n      \"content\":\"Possimus excepturi voluptatem unde incidunt debitis optio...\",\n      \"created_at\":\"03 Jul 2021 12:52 pm\",\n      \"post_id\":25,\n      \"user_id\":2,\n      \"user\":\"Mr. Johnathan Durgan MD\"\n   }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/API/V1/CommentsController.php",
    "groupTitle": "Comments"
  },
  {
    "type": "get",
    "url": "/posts/{id}/comments",
    "title": "Get Comments",
    "version": "1.0.0",
    "name": "GetComments",
    "group": "Comments",
    "description": "<p>Get the comments of a post identified by <code>id</code>.</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p><a href=\"#success-examples-Auth-GetAccessToken-1_0_0-0\">Access token</a> in the format: <code>Bearer {access-token}</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p><code>application/json</code></p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "Request Example",
          "content": "http://localhost:8000/api/v1/posts/34/comments",
          "type": "url"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data",
            "description": "<p>List of Comments. See <a href=\"#api-Comments-GetComment\">Comment details</a></p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"data\": [\n        {... comment ...},\n        {... comment ...}\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/API/V1/CommentsController.php",
    "groupTitle": "Comments"
  },
  {
    "type": "put",
    "url": "/comments/{id}",
    "title": "Update Comment",
    "version": "1.0.0",
    "name": "UpdateComment",
    "group": "Comments",
    "description": "<p>Update comment details.</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p><a href=\"#success-examples-Auth-GetAccessToken-1_0_0-0\">Access token</a> in the format: <code>Bearer {access-token}</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p><code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p><code>application/json</code></p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>ID of the comment</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>Content of the comment</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"content\":\"Possimus excepturi voluptatem unde incidunt debitis optio...\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Comment details</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.id",
            "description": "<p>ID of the comment</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.content",
            "description": "<p>Content of the comment</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.created_at",
            "description": "<p>Comment created time in the format <code>03 Jul 2021 12:52 pm</code></p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.post_id",
            "description": "<p>ID of the post that the comment belongs to</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.user_id",
            "description": "<p>User ID of the comment owner</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.user",
            "description": "<p>Name of the comment owner</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success Response",
          "content": "{\n   \"data\":{\n      \"id\":1,\n      \"content\":\"Possimus excepturi voluptatem unde incidunt debitis optio...\",\n      \"created_at\":\"03 Jul 2021 12:52 pm\",\n      \"post_id\":25,\n      \"user_id\":2,\n      \"user\":\"Mr. Johnathan Durgan MD\"\n   }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/API/V1/CommentsController.php",
    "groupTitle": "Comments"
  },
  {
    "type": "post",
    "url": "/posts",
    "title": "Add New Post",
    "version": "1.0.0",
    "name": "CreatePost",
    "group": "Posts",
    "description": "<p>Saves post details.</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p><a href=\"#success-examples-Auth-GetAccessToken-1_0_0-0\">Access token</a> in the format: <code>Bearer {access-token}</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p><code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p><code>application/json</code></p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Post title</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "body",
            "description": "<p>Content of the post</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"title\":\"Reprehenderit aut atque sed voluptatibus aut.\",\n    \"body\":\"Possimus excepturi voluptatem unde incidunt debitis optio...\",\n }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Post details</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.id",
            "description": "<p>ID of the post</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.title",
            "description": "<p>Post title</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.body",
            "description": "<p>Content of the post</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.created_at",
            "description": "<p>Post created time in the format <code>03 Jul 2021 12:52 pm</code></p>"
          },
          {
            "group": "Success 200",
            "type": "decimal",
            "optional": false,
            "field": "data.status",
            "description": "<p>Status of the post, <code>0</code> for <code>New</code> and <code>1</code> for <code>Approved</code></p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.user_id",
            "description": "<p>User ID of the post owner</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.user",
            "description": "<p>Name of the post owner</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success Response",
          "content": "{\n   \"data\":{\n      \"id\":1,\n      \"title\":\"Reprehenderit aut atque sed voluptatibus aut.\",\n      \"body\":\"Possimus excepturi voluptatem unde incidunt debitis optio...\",\n      \"created_at\":\"03 Jul 2021 12:52 pm\",\n      \"status\":1,\n      \"user_id\":2,\n      \"user\":\"Mr. Johnathan Durgan MD\"\n   }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/API/V1/PostsController.php",
    "groupTitle": "Posts"
  },
  {
    "type": "get",
    "url": "/posts/{id}",
    "title": "Get Post",
    "version": "1.0.0",
    "name": "GetPost",
    "group": "Posts",
    "description": "<p>Get post details.</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p><a href=\"#success-examples-Auth-GetAccessToken-1_0_0-0\">Access token</a> in the format: <code>Bearer {access-token}</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p><code>application/json</code></p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>ID of the post</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Post details</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.id",
            "description": "<p>ID of the post</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.title",
            "description": "<p>Post title</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.body",
            "description": "<p>Content of the post</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.created_at",
            "description": "<p>Post created time in the format <code>03 Jul 2021 12:52 pm</code></p>"
          },
          {
            "group": "Success 200",
            "type": "decimal",
            "optional": false,
            "field": "data.status",
            "description": "<p>Status of the post, <code>0</code> for <code>New</code> and <code>1</code> for <code>Approved</code></p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.user_id",
            "description": "<p>User ID of the post owner</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.user",
            "description": "<p>Name of the post owner</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success Response",
          "content": "{\n   \"data\":{\n      \"id\":1,\n      \"title\":\"Reprehenderit aut atque sed voluptatibus aut.\",\n      \"body\":\"Possimus excepturi voluptatem unde incidunt debitis optio...\",\n      \"created_at\":\"03 Jul 2021 12:52 pm\",\n      \"status\":1,\n      \"user_id\":2,\n      \"user\":\"Mr. Johnathan Durgan MD\"\n   }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/API/V1/PostsController.php",
    "groupTitle": "Posts"
  },
  {
    "type": "get",
    "url": "/posts",
    "title": "Get Posts",
    "version": "1.0.0",
    "name": "GetPosts",
    "group": "Posts",
    "description": "<p>Get the list of posts. This will return only the posts accessible to user identified by the provided <code>access-token</code>. Pagination is supported using <code>page</code> and <code>per_page</code> query parameters. Searching is supported using <code>search</code> parameter.</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p><a href=\"#success-examples-Auth-GetAccessToken-1_0_0-0\">Access token</a> in the format: <code>Bearer {access-token}</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p><code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p><code>application/json</code></p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "page",
            "description": "<p>Number of the page</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "per_page",
            "description": "<p>Number of posts per page</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "search",
            "description": "<p>Keywords to search the posts by <code>title</code>, <code>body</code> &amp; <code>user name</code></p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "status",
            "description": "<p>Status: <code>0</code> for 'New', <code>1</code> for <code>Approved</code>. This parameter effects only when the requesting user is an <code>admin</code>, ignored otherwise.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "http://localhost:8000/api/v1/posts?page=1&per_page=20",
          "type": "url"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data",
            "description": "<p>List of Posts. See <a href=\"#api-Posts-GetPost\">Post details</a></p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "links",
            "description": "<p>Pagination links. Check <a href=\"#api-_footer\">pagination details</a> below for more information.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "meta",
            "description": "<p>Meta details of current page. Check <a href=\"#api-_footer\">pagination details</a> below for more information.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"data\": [\n        {... post ...},\n        {... post ...}\n    ],\n    \"links\": {\n        \"first\": \"http://localhost:8000/api/v1/posts?page=1\",\n        \"last\": \"http://localhost:8000/api/v1/posts?page=1\",\n        \"prev\": null,\n        \"next\": null\n    },\n    \"meta\": {\n        \"current_page\": 1,\n        \"from\": 1,\n        \"last_page\": 1,\n        \"path\": \"http://localhost:8000/api/v1/posts\",\n        \"per_page\": 15,\n        \"to\": 1,\n        \"total\": 1\n    }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/API/V1/PostsController.php",
    "groupTitle": "Posts"
  },
  {
    "type": "put",
    "url": "/posts/{id}",
    "title": "Update Post",
    "version": "1.0.0",
    "name": "UpdatePost",
    "group": "Posts",
    "description": "<p>Update post details.</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p><a href=\"#success-examples-Auth-GetAccessToken-1_0_0-0\">Access token</a> in the format: <code>Bearer {access-token}</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p><code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p><code>application/json</code></p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>ID of the post</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "title",
            "description": "<p>Post title</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "body",
            "description": "<p>Content of the post</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "status",
            "description": "<p>Status of the post, <code>0</code> for <code>New</code> and <code>1</code> for <code>Approved</code>. This attribute will be saved only of the requesting user is an <code>admin</code>, it will be ignored otherwise.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Example",
          "content": "{\n    \"title\":\"Reprehenderit aut atque sed voluptatibus aut.\",\n    \"body\":\"Possimus excepturi voluptatem unde incidunt debitis optio...\",\n    \"status\":1\n }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Post details</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.id",
            "description": "<p>ID of the post</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.title",
            "description": "<p>Post title</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.body",
            "description": "<p>Content of the post</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.created_at",
            "description": "<p>Post created time in the format <code>03 Jul 2021 12:52 pm</code></p>"
          },
          {
            "group": "Success 200",
            "type": "decimal",
            "optional": false,
            "field": "data.status",
            "description": "<p>Status of the post, <code>0</code> for <code>New</code> and <code>1</code> for <code>Approved</code></p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.user_id",
            "description": "<p>User ID of the post owner</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.user",
            "description": "<p>Name of the post owner</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success Response",
          "content": "{\n   \"data\":{\n      \"id\":1,\n      \"title\":\"Reprehenderit aut atque sed voluptatibus aut.\",\n      \"body\":\"Possimus excepturi voluptatem unde incidunt debitis optio...\",\n      \"created_at\":\"03 Jul 2021 12:52 pm\",\n      \"status\":1,\n      \"user_id\":2,\n      \"user\":\"Mr. Johnathan Durgan MD\"\n   }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/API/V1/PostsController.php",
    "groupTitle": "Posts"
  }
] });
