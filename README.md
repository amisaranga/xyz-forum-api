# XYZ Forum API

This is the back-end API for the [XYZ Forum app](https://bitbucket.org/amisaranga/xyz-forum).
This application is developed using **Laravel** PHP framework (version 8.x).

## Project setup

This application requires following software to setup and run:

- Git
- PHP 7.3 or later
- Composer 2.x
- MySql 5.7 / MariaDB 10.2

### Install

Clone the repository:

```
git clone https://bitbucket.org/amisaranga/xyz-forum-api.git
```

Move in to the project folder:

```
cd xyz-forum-api
```

Install packages:

```
composer install
```

### Configure

Open `.env` file in the project folder using a plain text editor.
If this file is not available. Create it using:

```
cp .env.example .env
```

Generate the app key:

```
php artisan key:generate
```

Set `APP_URL`

```
APP_URL=http://localhost:8000
```

Create a MySql / MariaDB database and add details to `.env` file:

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=db_name
DB_USERNAME=db_user
DB_PASSWORD=db_user_password
```

Replace `db_name`, `db_user` and `db_user_password` with the respective values.

Then run following command to prepare the database:

```
php artisan migrate
```

Then seed test data:

```
php artisan db:seed
```

This will add a number of posts, comments and users to the database. The first
user in `users` table is an `admin` user. Which can be used for testing
administrator functions.

Admin user login email: `admin@xyz.forum`

Default password for all users is `password`

### Run

For a test run, application can be run using:

```
php artisan serve
```

This will serve the application at `http://localhost:8000/`.

API base URL is `http://localhost:8000/api/v1/`. Use this for setting up
front-end application.

API documentation is available at `http://localhost:8000/docs/api/index.html`


### Front-end application

Instructions for setting up the front-end application can be found [here](https://bitbucket.org/amisaranga/xyz-forum/src/master/README.md)
